<?php

/**
 * @file
 * Admin functions for Hipchat Keyword Response.
 */

/**
 * Implements hook_form().
 */
function hipchat_keyword_response_form($form, &$form_state, $hipchat_keyword_response = NULL) {
  $form = array();

  $form['keyword'] = array(
    '#title' => t('Keyword'),
    '#type' => 'textfield',
    '#default_value' => isset($hipchat_keyword_response->keyword) ? $hipchat_keyword_response->keyword : '',
    '#description' => t('The keyword Hipchat should respond to. Separate multiple input phrases with commas. Examples: <i>hi, hello, yo<i>.'),
    '#required' => TRUE,
    '#maxlength' => 255,
    '#weight' => -1,
  );

  field_attach_form('hipchat_keyword_response', $hipchat_keyword_response, $form, $form_state);

  $form['actions'] = array(
    '#type' => 'actions',
    'submit' => array(
      '#type' => 'submit',
      '#value' => isset($hipchat_keyword_response->keyword_response_id) ? t('Update') : t('Save'),
    ),
    'delete_link' => array(
      '#markup' => isset($hipchat_keyword_response->keyword_response_id) ?
      l(
          t('Delete'),
          'admin/content/hipchat-keyword-response/manage/' . $hipchat_keyword_response->keyword_response_id . '/delete',
          array(
            'attributes' => array(
              'id' => array('hipchat_keyword_response-delete-' . $hipchat_keyword_response->keyword_response_id),
              'class' => array('button remove'),
            ), 'query' => array('destination' => 'admin/content/hipchat_keyword_response'),
          )
      )
      : '',
    ),
  );
  return $form;
}

/**
 * Implements hook_form_validate().
 */
function hipchat_keyword_response_form_validate($form, &$form_state) {
}

/**
 * Implements hook_form_submit().
 */
function hipchat_keyword_response_form_submit($form, &$form_state) {
  $hipchat_keyword_response = entity_ui_form_submit_build_entity($form, $form_state);
  $hipchat_keyword_response->save();
  drupal_set_message(t('Hipchat Keyword Response has been saved.'));
  $form_state['redirect'] = 'admin/content/hipchat-keyword-response';
}
